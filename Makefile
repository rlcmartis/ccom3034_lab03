listClient: listClient.o list.o
	g++ -o listClient listClient.o list.o

listClient.o: listClient.cpp list.h
	g++ -c listClient.cpp

list.o: list.cpp list.h
	g++ -c list.cpp

clean:
	rm *.o listClient
